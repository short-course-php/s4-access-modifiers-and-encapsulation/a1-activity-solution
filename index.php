<?php require_once("./code.php"); ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>S4 Access Modifiers and Encapsulation</title>
</head>
<body>

	<div style="display: flex; justify-content: space-evenly;">
		<div>
			<h3 style="text-align: center;">Building</h3>
			<p> <?php echo "The name of the building is " . $building->getName(); ?></p>
			<p> <?php echo "The " . $building->getName() . " has " . $building->getFloors() . " floors"; ?></p>
			<p> <?php echo "The " . $building->getName() . " is located at " . $building->getAddress(); ?></p>
			<?php $building->setName('Caswynn Complex'); ?>
			<p> <?php echo "The name of the building has been changed to " . $building->getName(); ?></p>

		</div>

		<div>
			<h3 style="text-align: center;">Condominium</h3>
			<p> <?php echo "The name of the condominium is " . $condo->getName(); ?></p>
			<p> <?php echo "The " . $condo->getName() . " has " . $condo->getFloors() . " floors"; ?></p>
			<p> <?php echo "The " . $condo->getName() . " is located at " . $condo->getAddress(); ?></p>
			<?php $condo->setName('Enzo Tower'); ?>
			<p> <?php echo "The name of the condominium has been changed to " . $condo->getName(); ?></p>
		</div>

	</div>

</body>
</html>